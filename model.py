import tensorflow as tf
import numpy as np
from numpy.linalg import svd, eig
import pylab as pl
from copy import deepcopy
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from IPython import display
from matplotlib import pyplot

# variable initialization functions
def weight_variable(shape):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)

def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)

# plotting functions
def make_mosaic(imgs, nrows, ncols, border=1):
    nimgs = imgs.shape[0]
    imshape = imgs.shape[1:]
    mosaic = np.ma.masked_all((nrows * imshape[0] + (nrows - 1) * border,
                            ncols * imshape[1] + (ncols - 1) * border),
                            dtype=np.float32)
    paddedh = imshape[0] + border
    paddedw = imshape[1] + border
    for i in range(nimgs):
        row = int(np.floor(i / ncols))
        col = i % ncols
        mosaic[row * paddedh:row * paddedh + imshape[0],
               col * paddedw:col * paddedw + imshape[1]] = imgs[i]
    return mosaic

def nice_imshow(ax, data, vmin=None, vmax=None, cmap=None):
    if cmap is None:
        cmap = "inferno"
    if vmin is None:
        vmin = data.min()
    if vmax is None:
        vmax = data.max()
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    im = ax.imshow(data, vmin=vmin, vmax=vmax, interpolation='nearest', cmap=cmap)
    pl.colorbar(im, cax=cax)

# classification accuracy plotting
def plot_test_acc(plot_handles):
    plt.legend(handles=plot_handles, loc="center right")
    plt.xlabel("Iterations")
    plt.ylabel("Test Accuracy")
    plt.ylim(0,1)
    display.display(plt.gcf())
    display.clear_output(wait=True)

class Model:
    
    def __init__(self, x, y_):
        in_dim = int(x.get_shape()[1]) # 784 for MNIST
        out_dim = int(y_.get_shape()[1]) # 10 for MNIST
        
        self.x = x # input placeholder
        
        # simple 2-layer network
        # W1 = weight_variable([in_dim,50])
        # b1 = bias_variable([50])
        
        # W2 = weight_variable([50,out_dim])
        # b2 = bias_variable([out_dim])
        
        # h1 = tf.nn.relu(tf.matmul(x,W1) + b1) # hidden layer
        # self.y = tf.matmul(h1,W2) + b2 # output layer
        
        # self.var_list = [W1, b1, W2, b2]
        
        
        W1 = weight_variable([in_dim, out_dim])
        b1 = bias_variable([out_dim])
        
        self.y = tf.matmul(x,W1) + b1 # hidden layer
        
        self.var_list = [W1, b1]
        
        # vanilla single-task loss
        self.cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=self.y))
        self.set_vanilla_loss()
        
        # performance metrics
        correct_prediction = tf.equal(tf.argmax(self.y,1), tf.argmax(y_,1))
        self.accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    
    # train/compare vanilla sgd and ewc
    def train_task(self, sess, num_iter, disp_freq, trainset, testsets, x, y_, lams=[0]):
        for l in range(len(lams)):
            # lams[l] sets weight on old task(s)
            self.restore(sess) # reassign optimal weights from previous training session
            if(lams[l] == 0):
                self.set_vanilla_loss()
            else:
                self.update_ewc_loss(lams[l])
            # initialize test accuracy array for each task 
            test_accs = []
            for task in range(len(testsets)):
                test_accs.append(np.zeros(int(num_iter/disp_freq)))
            # train on current task
            for iter in range(num_iter):
                batch = trainset.train.next_batch(100)
                self.train_step.run(feed_dict={x: batch[0], y_: batch[1]})
                if iter % disp_freq == 0:
                    plt.subplot(1, len(lams), l+1)
                    plots = []
                    colors = ['r', 'b', 'g']
                    for task in range(len(testsets)):
                        feed_dict={x: testsets[task].test.images, y_: testsets[task].test.labels}
                        test_accs[task][int(iter/disp_freq)] = self.accuracy.eval(feed_dict=feed_dict)
                        c = chr(ord('A') + task)
                        plot_h, = plt.plot(range(1,iter+2,disp_freq), test_accs[task][:int(iter/disp_freq)+1], colors[task], label="task " + c)
                        plots.append(plot_h)
                    plot_test_acc(plots)
                    if l == 0: 
                        plt.title("vanilla sgd")
                    else:
                        plt.title("ewc")
                    plt.gcf().set_size_inches(len(lams)*5, 3.5)

    def compute_fisher(self, imgset, sess, num_samples=200, plot_diffs=False, disp_freq=10):
        # computer Fisher information for each parameter
        
        # initialize Fisher information for most recent task
        self.f_accum = []
        for v in range(len(self.var_list)):
            self.f_accum.append(np.zeros(self.var_list[v].get_shape().as_list()))
            
        # sampling a random class from softmax
        probs = tf.nn.softmax(self.y)
        class_ind = tf.to_int32(tf.multinomial(tf.log(probs), 1)[0][0])
        
        if(plot_diffs):
            # track differences in mean Fisher info
            F_prev = deepcopy(self.f_accum)
            mean_diffs = np.zeros(0)
            
        for i in range(num_samples):
            # select random input image
            im_ind = np.random.randint(imgset.shape[0])
            # compute first-order derivatives
            ders = sess.run(tf.gradients(tf.log(probs[0,class_ind]), self.var_list), feed_dict={self.x: imgset[im_ind:im_ind+1]})
            # square the derivatives and add to total
            for v in range(len(self.f_accum)):
                self.f_accum[v] += np.square(ders[v])
            if(plot_diffs):
                if i % disp_freq == 0 and i > 0:
                    # recording mean diffs of F
                    F_diff = 0
                    for v in range(len(self.f_accum)):
                        F_diff += np.sum(np.absolute(self.f_accum[v]/(i+1) - F_prev[v]))
                    mean_diff = np.mean(F_diff)
                    mean_diffs = np.append(mean_diffs, mean_diff)
                    for v in range(len(self.f_accum)):
                        F_prev[v] = self.f_accum[v]/(i+1)
                    plt.plot(range(disp_freq+1, i+2, disp_freq), mean_diffs)
                    plt.xlabel("Number of samples")
                    plt.ylabel("Mean absolute Fisher difference")
                    display.display(plt.gcf())
                    display.clear_output(wait=True)
                    
        # divide totals by number of samples
        for v in range(len(self.f_accum)):
            self.f_accum[v] /= num_samples
    
    def compute_svd(self, layer, steps):
        # format matrices
        w_tot = self.var_list[layer].eval()
        dim = [(np.sqrt(w_tot.shape[0])).astype(int)] * 2 + [w_tot.shape[1]]
        w = np.transpose(np.reshape(w_tot, (dim[0], dim[1], dim[2])), (2, 0, 1))
        # get eigen values
        self.eigv = []
        for k in range(dim[2]):
            eigv, _ = eig(w[k])
            self.eigv.append(eigv)
        np.asarray(self.eigv)
        # complex space x + yj
        x_max = 2
        x_min = -2
        y_max = 2
        y_min = -2
        x = np.linspace(x_min, x_max, steps)
        y = np.linspace(y_min, y_max, steps)
        self.spectrum = np.empty([dim[2], steps, steps]) 
        eye = np.eye(dim[0], dim[1])
        # compute pseudospectrum
        for k in range(dim[2]):
            for i in range(steps):
                for j in range(steps):
                    sv = svd((x[i] + y[j] * 1j) * eye - w[k], compute_uv=0)
                    self.spectrum[k][i][j] = sv[-1]
    
    def star(self):
        # used for saving optimal weights after most recent task training
        self.star_vars = []
        
        for v in range(len(self.var_list)):
            self.star_vars.append(self.var_list[v].eval())
    
    def restore(self, sess):
        # reassign optimal weights for latest task
        if hasattr(self, "star_vars"):
            for v in range(len(self.var_list)):
                sess.run(self.var_list[v].assign(self.star_vars[v]))
                
    def set_vanilla_loss(self):
        self.train_step = tf.train.GradientDescentOptimizer(0.1).minimize(self.cross_entropy)
        
    def update_ewc_loss(self, lam):
        # elastic weight consolidation
        # lam is weighting for previous task(s) constraints
        
        if not hasattr(self, "ewc_loss"):
            self.ewc_loss = self.cross_entropy
            
        for v in range(len(self.var_list)):
            self.ewc_loss += (lam/2) * tf.reduce_sum(tf.multiply(self.f_accum[v].astype(np.float32),tf.square(self.var_list[v] - self.star_vars[v])))
        self.train_step = tf.train.GradientDescentOptimizer(0.1).minimize(self.ewc_loss)
    
    def visualize(self, layer=0):
        w_tot = self.var_list[layer].eval()
        dim = [(np.sqrt(w_tot.shape[0])).astype(int)] * 2 + [w_tot.shape[1]]
        pl.figure(figsize=(10, 1.5 * dim[2]))
        
        # raw weights
        plt.subplot(1, 3, 1)
        x = np.transpose(np.reshape(w_tot, (dim[0], dim[1], dim[2])), (2, 0, 1))
        plt.axis('off')
        plt.title("Raw weights")
        nice_imshow(pl.gca(), make_mosaic(x, dim[2], 1), cmap="inferno")
        
        # fisher
        plt.subplot(1, 3, 2)
        x = np.transpose(np.reshape(self.f_accum[layer], (dim[0], dim[1], dim[2])), (2, 0, 1))
        plt.axis('off')
        plt.title("Fisher")
        nice_imshow(pl.gca(), make_mosaic(x, dim[2], 1), cmap="inferno")
        
        # pseudo-spectra
        plt.subplot(1, 3, 3)
        x = np.log10(np.transpose(self.spectrum, (0, 2, 1)))
        plt.axis('off')
        plt.title("Pseudospectrum")
        nice_imshow(pl.gca(), make_mosaic(x, dim[2], 1), cmap="inferno")    
