from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numbers
import numpy as np
import tensorflow as tf
from tensorflow.python.framework import ops
from tensorflow.python.ops import array_ops


def selective_dropout(x, seed=None, name=None):
  with ops.name_scope(name, "dropout", [x]) as name:
    x = ops.convert_to_tensor(x, name="x")
    if not x.dtype.is_floating:
      raise ValueError("x has to be a floating point tensor since it's going to"
                       " be scaled. Got a %s tensor instead." % x.dtype)

    shape = array_ops.shape(x)
    #print(tf.argmax(x, axis=1, output_type=tf.int32))
    # vals_and_inds = tf.nn.top_k(x, k=1)
    # print(vals_and_inds)
    ind = tf.argmax(x, axis=0)
    print(ind.eval())
    #mask = tf.scatter_nd([[ind]], [1.0], [20])
    mask = tf.ones(shape)

    ret = x * mask
    
    ret.set_shape(x.get_shape())
    return ret
